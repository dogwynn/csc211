#!/usr/bin/env bash

virtualenv -p python3.5 flask
source flask/bin/activate

pip install flask
pip install flask-login
pip install flask-openid
pip install flask-mail
pip install flask-sqlalchemy
pip install sqlalchemy-migrate
pip install flask-whooshalchemy
pip install flask-wtf
pip install flask-babel
pip install guess_language
pip install flipflop
pip install coverage
pip install flask-migrate
pip install flask-script
pip install ipython
