# CSC211 Final Project: 1611 King James Bible Web App

This code provides the basic structure for the final project. If you
choose to use this codebase, the highest grade you can expect is
a 90. However, if you can get this running, you are guaranteed to get
at least a 70.

If you already have work on the project you wish to keep, you'll need
to be carefull with the below instructions. That is, the below
instructions *might overwrite your existing work*. 

## Installation

### 1. Clone GitHub repo

In your PythonAnywhere account, you will need to start up a bash
console.

```bash
$ cd ~
$ git clone https://github.com/dogwynn/csc211.git example-csc211
$ cd csc211/project
$ cp -r ~/example-csc211/project/* .
```

### 2. Setup the Python environment

From your PythonAnywhere bash console:

```bash
$ cd ~/csc211/project
$ ./setup_python.sh
```

This will create a `~/csc211/project/flask` directory. This directory
is a "virtual environment" or "virtualenv" that provides all of the
Python libraries necessary to run your Flask web app. 

In your PythonAnywhere "Web" tab, you'll need to connect your web app
to this Virtualenv directory.

- Look for the **"Virtualenv:"** section of the Web tab page.
- Look for the *"Enter path to a virtualenv, if desired"* text and
  click it
- Type out `/home/<userid>/csc211/project/flask` in the input box
  (where `<userid>` is your PythonAnywhere ID)
- Reload your web app

At this point, you should be able to access your web app's URL:

http://&lt;userid&gt;.pythonanywhere.com

### 3. Edit your `views.py` module

Open up `~/csc211/project/app/views.py` for editing. You'll need to
fill in the following functions/routes:

- **`index`**: This should return a call to `render_template` with the
  appropriate arguments. See the docstring for `index` (as well as the
  `index.html.j2` template) for more details.
- **`book_route`**: You will need to populate the `verses` list with
  those verses whose book equals the parameter `book`
- **`chapter_route`**: You will need to populate the `verses` list
  with those verses whose book equals the parameter `book` and whose
  chapter number equals the parameter `chapter`
- **`verse_route`**: You will need to populate the `verses` list with
  those verses whose book equals the parameter `book` and whose
  chapter number equals the parameter `chapter` and whose verse number
  equals the parameter `verse`

### 4. Restart your web app

On your Web tab, click the green restart button. If you've added the
correct code to your `views.py` module, then you should see the books
of the Bible at `/index` and you should be able to click around theses
books, chapters and verses.

Your workflow will consist of changing `views.py` and restarting your
web app. **Remember**: every time you change a Python file
(e.g. `views.py`), you have to restart your web app. However, you
*don't* have to restart if you change a template. 

## Next Steps

### RDBMS functionality

There are other files/modules available to use to create and populate
a SQLite database.

- Look at the `app/models.py` module. This defines Flask-SQLAlchemy
  models that can be used to query the database for verses.
- Look at the `db_create.py` script. This can be run to create the
  database. You can then use this database model (imported into
  `views.py` as `db`) to query the verses.

### Testing locally (i.e. not on PythonAnywhere)

If you wish to do testing on your laptop before deploying to
PythonAnywhere, you can. The following command is what you would use
to do so.

To start your server:

```bash
$ cd ~/csc211/project
$ ./manage.py runserver --debug --restart
```

This will run your server in the terminal window. If there's a error
in your code, it will print out the error to the browser. It will also
restart your webserver whenever you change any Python code referenced
from your Flask app.



