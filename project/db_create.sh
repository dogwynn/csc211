#!/usr/bin/env bash
./manage.py db init
./manage.py db migrate
./manage.py db upgrade
./manage.py populate_db
