from flask_wtf import Form
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired, Email


class LoginForm(Form):
    nickname = StringField('nickname')
    email = StringField('nickname', validators=[DataRequired(), Email()])
    remember_me = BooleanField('remember_me', default=False)
