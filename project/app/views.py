from flask import (
    render_template, flash, redirect, url_for, request,
    session, g, 
)
from flask_login import (
    login_user, logout_user, current_user, login_required
)
from app import app, login_manager, db
from .forms import LoginForm
from .models import Verse, User

import kjv

@app.route('/')
@app.route('/index')
def index():
    '''Render links to each book of the Bible

    Hint: You'll want to render_template the "index.html.j2"
    template. Look at that file for more information.

    Returns:
      str: The rendered template for the index page

    '''
        
    return 'Please fix...'

 
@app.before_request
def before_request():
    g.user = current_user

@login_manager.user_loader
def user_loader(str_uid):
    return User.query.get(int(str_uid))

@app.route('/login',methods=['GET','POST'])
def login():
    return redirect(url_for('index'))


@app.route('/book/<book>')
def book_route(book):
    '''Render all the verses from a single book of the Bible

    Args:
    - book (str): the abbreviated book name

    Hint: There is a template "verses.html.j2" that you could use to
    render the verses. Look at that file for more information about
    how to use it.

    Returns:
      str: The rendered template as a string

    '''
    verses = []
    
    return render_template(
        'verses.html.j2',
        title=kjv.books[book],
        verses=verses,
    )

@app.route('/chapter/<book>/<int:chapter>')
def chapter_route(book, chapter):
    '''Render all the verses from a single chapter of a single book of the
    Bible

    Args:
    - book (str): the abbreviated book name
    - chapter (int): the chapter number

    Hint: There is a template "verses.html.j2" that you could use to
    render the verses. Look at that file for more information about
    how to use it.

    Returns:
      str: The rendered template as a string

    '''
    verses = []
    
    return render_template(
        'verses.html.j2',
        title=kjv.books[book],
        verses=verses,
    )

@app.route('/verse/<book>/<int:chapter>/<int:verse>')
def verse_route(book, chapter, verse):
    '''Render a single verse of the Bible

    Args:
    - book (str): the abbreviated book name
    - chapter (int): the chapter number
    - verse (int): the verse number

    Hint: There is a template "verses.html.j2" that you could use to
    render the verses. Look at that file for more information about
    how to use it.

    Returns:
      str: The rendered template as a string

    '''
    verses = []
    
    return render_template(
        'verses.html.j2',
        title=kjv.books[book],
        verses=verses,
    )

@app.route('/bookmark/<book>/<int:chapter>/<int:verse>')
@login_required
def bookmark_route(book, chapter, verse):
    return redirect(url_for('index'))

@app.route('/bookmarks')
@login_required
def bookmarks_route():
    return redirect(url_for('index'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))
