from app import db


class Verse(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    book = db.Column(db.String(5), index=True)
    chapter = db.Column(db.Integer, index=True)
    verse = db.Column(db.Integer, index=True)
    text = db.Column(db.String())

    def __repr__(self):
        return '<Verse book:{} ch:{} v:{} -- {}>'.format(
            self.book, self.chapter, self.verse,
            self.text[:10]
        )

bookmarks = db.Table(
    'bookmarks',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('verse_id', db.Integer, db.ForeignKey('verse.id')),
)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    nickname = db.Column(db.String(64), index=True)
    email = db.Column(db.String(120), index=True, unique=True)

    bookmarks = db.relationship(
        'Verse', secondary=bookmarks,
        backref=db.backref('users', lazy='dynamic'),
    )

    def __repr__(self):
        return '<User {}>'.format(self.nickname)

    @property
    def is_authenticated(self):
        return True
    @property
    def is_active(self):
        return True
    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)
