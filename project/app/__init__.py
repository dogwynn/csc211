from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import flask_script
from flask_migrate import Migrate, MigrateCommand
from flask_login import LoginManager

from config import SQLALCHEMY_DATABASE_URI

app = Flask(__name__)
app.config.from_object('config')

# flask-sqlalchemy setup
db = SQLAlchemy(app)

# flask-migrate
migrate = Migrate(app, db)

# flask-script script management
script_manager = flask_script.Manager(app)
# flask-script commands 
script_manager.add_command('db', MigrateCommand)

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

from app import views, models

