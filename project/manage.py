#!flask/bin/python3
from app import app, script_manager, db
from app.models import User, Verse
import kjv

@script_manager.command
def populate_db():
    verses = kjv.kjv_verses()
    for v in verses:
        verse = Verse(**v._asdict())
        db.session.add(verse)
    db.session.commit()



if __name__=='__main__':
    script_manager.run()
