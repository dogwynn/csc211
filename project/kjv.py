import bz2
from collections import namedtuple, OrderedDict

Verse = namedtuple('Verse',['book','chapter','verse','text'])

def kjv_verses():
    '''Return a list of Verse objects from the kjv.tsv file

    Each object returned can be referenced both as a tuple and as an
    object with the attributes: book, chapter, verse, text.

    Example:

        >>> import kjv
        >>> verses = kjv.kjv_verses()
        >>> v = verses[0]
        >>> v[0]
        'ge'
        >>> v.book
        'ge'
        >>> v[1]
        0
        >>> v.chapter
        0

    '''
    def parse_line(line):
        book, chapter, verse, text = line.strip().split('\t')
        text = text.replace('<','&lt;')
        text = text.replace('>','&gt;')
        return Verse(book, int(chapter), int(verse), text)
    
    with bz2.open('kjv.tsv.bz2','rt') as rfp:
        verses = [parse_line(line) for line in rfp]
    return verses

books = OrderedDict(
    [('ge', 'Genesis'), ('ex', 'Exodus'), ('le', 'Leviticus'),
     ('nu', 'Numbers'),('de', 'Deuteronomy'), ('jos', 'Joshua'),
     ('jg', 'Judges'), ('ru', 'Ruth'), ('1sa', '1 Samuel'), ('2sa', '2 Samuel'),
     ('1ki', '1 Kings'), ('2ki', '2 Kings'), ('1ch', '1 Chronicles'),
     ('2ch', '2 Chronicles'), ('ezr', 'Ezra'), ('ne', 'Nehemiah'),
     ('es', 'Esther'), ('job', 'Job'), ('ps', 'Psalms'), ('pr', 'Proverbs'),
     ('ec', 'Ecclesiastes'), ('song', 'Song of Solomon'), ('isa', 'Isaiah'),
     ('jer', 'Jeremiah'), ('la', 'Lamentations'), ('eze', 'Ezekiel'),
     ('da', 'Daniel'), ('ho', 'Hosea'), ('joe', 'Joel'), ('am', 'Amos'),
     ('ob', 'Obadiah'), ('jon', 'Jonah'), ('mic', 'Micah'), ('na', 'Nahum'),
     ('hab', 'Habakkuk'), ('zep', 'Zephaniah'), ('hag', 'Haggai'),
     ('zec', 'Zechariah'), ('mal', 'Malachi'),

     ('mt', 'Matthew'), ('mr', 'Mark'), ('lu', 'Luke'), ('joh', 'John'),
     ('ac', 'Acts'), ('ro', 'Romans'), ('1co', '1 Corinthians'),
     ('2co', '2 Corinthians'), ('ga', 'Galatians'), ('eph', 'Ephesians'),
     ('php', 'Philippians'), ('col', 'Colossians'), ('1th', '1 Thessalonians'),
     ('2th', '2 Thessalonians'), ('1ti', '1 Timothy'), ('2ti', '2 Timothy'),
     ('tit', 'Titus'), ('phm', 'Philemon'), ('heb', 'Hebrews'),
     ('jas', 'James'), ('1pe', '1 Peter'), ('2pe', '2 Peter'),
     ('1jo', '1 John'), ('2jo', '2 John'), ('3jo', '3 John'),
     ('jude', 'Jude'), ('re', 'Revelation')]
)

def add_full_name(abbrev_list):
    if type(abbrev_list[0]) is str:
        return [(abbrev, books[abbrev]) for abbrev in abbrev_list]
    else:
        return [add_full_name(l) for l in abbrev_list]

old_testament = add_full_name([
    'ge', 'ex', 'le', 'nu', 'de', 'jos', 'jg', 'ru', '1sa', '2sa',
    '1ki', '2ki', '1ch', '2ch', 'ezr', 'ne', 'es', 'job', 'ps', 'pr',
    'ec', 'song', 'isa', 'jer', 'la', 'eze', 'da', 'ho', 'joe', 'am',
    'ob', 'jon', 'mic', 'na', 'hab', 'zep', 'hag', 'zec', 'mal',
])
new_testament = add_full_name([
    'mt', 'mr', 'lu', 'joh', 'ac', 'ro', '1co', '2co', 'ga', 'eph',
    'php', 'col', '1th', '2th', '1ti', '2ti', 'tit', 'phm', 'heb',
    'jas', '1pe', '2pe', '1jo', '2jo', '3jo', 'jude', 're',
])

old_testament_grid = add_full_name([
    ['ge', 'ex', 'le', 'nu', 'de', 'jos', ],
    ['jg', 'ru', '1sa', '2sa', '1ki', '2ki', ],
    ['1ch', '2ch', 'ezr', 'ne', 'es', 'job', ],
    ['ps', 'pr', 'ec', 'song', 'isa', 'jer', ],
    ['la', 'eze', 'da', 'ho', 'joe', 'am', ],
    ['ob', 'jon', 'mic', 'na', 'hab', 'zep', ],
    ['hag', 'zec', 'mal', ],
])

new_testament_grid = add_full_name([
    ['mt', 'mr', 'lu', 'joh', 'ac', 'ro', ],
    ['1co', '2co', 'ga', 'eph', 'php', 'col', ],
    ['1th', '2th', '1ti', '2ti', 'tit', 'phm', ],
    ['heb', 'jas', '1pe', '2pe', '1jo', '2jo', ],
    ['3jo', 'jude', 're'],
])

grid = old_testament_grid + new_testament_grid
